import os

from pyrogram import Client, filters

text = "Данное сообщение (материал) создано " \
       "и (или) распространено иностранным средством массовой информации, " \
       "выполняющим функции иностранного агента"

api_id = int(os.getenv("API_ID"))
api_hash = os.getenv("API_HASH")
isBold = os.getenv("IS_BOLD").lower() == "true"
isItalic = os.getenv("IS_ITALIC").lower() == "true"
isUpper = os.getenv("IS_UPPER").lower() == "true"

if isBold:
    text = "<b>" + text + "</b>"
if isItalic:
    text = "<i>" + text + "</i>"
if isUpper:
    text = text.upper()


app = Client("my_account", api_id=api_id, api_hash=api_hash)

@app.on_message(filters.me)
async def my_handler(client, message):
    if message.text:
        await message.edit(message.text + "\n\n" + text)
    else:
        await message.edit(text)

app.run()