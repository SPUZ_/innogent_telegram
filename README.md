# innogent_telegram

## Добавляем к каждому сообщению в телеграмме плашку об иностранных агентах

![](mesage.jpeg)

## Начнем

### without Docker
Обязательно наличие Python3

Скачиваем
```
git clone https://gitlab.com/SPUZ_/innogent_telegram
```

Разворачиваем виртуальное окружение
```
pip3 -m venv venv
source venv/bin/activate
```

Меняем в системе окружения значения: API_ID, API_HASH, IS_BOLD, IS_ITALIC, IS_UPPER
Либо просто меняем в коде значения
```
api_id - надо получить у телеграмма
api_hash - тоже получаем у телеграмма
isBold - будет ли текст жирным
isItalic - будет ли текст курсивом
isUpper - будет ли текст полностью большими буквами
```
